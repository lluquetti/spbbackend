const express = require('express');
const expressValidator = require('express-validator');
const bodyParser = require('body-parser');
const consign = require('consign');
const DataStore = require('nedb');
const teste = require('./app/middleware/pet');

const app = express();

app.dataBase = new DataStore();

app.use(bodyParser.json());
app.use(expressValidator());
app.use(teste);
consign()
  .include('app/routes')
  .into(app);
app.listen(8080, () => {
  console.log('Server running on port 8080');
});

module.exports = app;
