const seller = require('../models/seller.js');

module.exports = (app) => {
  app.route('/Seller')
    .get((req, res) => {
      res.status(200).json(seller);
    });
};
