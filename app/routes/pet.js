const { body, validationResult } = require('express-validator/check');

const controller = require('../controller/pet_controller');

module.exports = (app) => {
  controller.fillValues(app);
  app.route('/pet')
    .get((req, res) => {
      controller.Get(app, req, res);
    })

    .post([
      body('name', 'Must be defined').exists(),
      body('category_id', 'Category must be dedined').exists(),
      body('category_id', 'Category must be integer value').isInt(),
      body('user_id', 'User must be dedined').exists(),
      body('user_id', 'Category must be integer value').isInt(),
    ], (req, res) => {
      const err = validationResult(req);
      if (!err.isEmpty()) {
        return res.status(400).json({ errors: err.array() });
      }
      return controller.Post(app, req, res);
    })

    .put([
      body('id', 'User must be dedined').exists(),
      body('id', 'Must be integer value').isString(),
      body('name', 'Must be defined').exists(),
      body('category_id', 'Must be dedined').exists(),
      body('category_id', 'Must be integer value').isInt(),
      body('user_id', 'Must be dedined').exists(),
      body('user_id', 'Must be integer value').isInt(),
    ], (req, res) => {
      const err = validationResult(req);
      if (!err.isEmpty()) {
        return res.status(400).json({ errors: err.array() });
      }
      return controller.Put(app, req, res);
    });

  app.route('/pet/:id')
    .get((req, res) => {
      controller.Search(app, req, res);
    })

    .delete((req, res) => {
      controller.Delete(app, req, res);
    });
};
