const categories = require('../models/categoty_pet.js');

module.exports = (app) => {
  app.route('/categories')
    .get((req, res) => {
      res.status(200).json(categories);
    });
};
