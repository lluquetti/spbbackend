const user = require('../models/user.js');

module.exports = (app) => {
  app.route('/Users')
    .get((req, res) => {
      user.pets = app.dataBase.getAllData();
      res.status(200).json(user);
    });
};
