const blog = require('../models/blog.js');

module.exports = (app) => {
  app.route('/blog')
    .get((req, res) => {
      res.status(200).json(blog);
    });
};
