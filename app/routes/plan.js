const plan = require('../models/plan.js');

module.exports = (app) => {
  app.route('/Plan')
    .get((req, res) => {
      res.status(200).json(plan);
    });
};
