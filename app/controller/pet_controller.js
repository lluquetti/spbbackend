const service = require('../services/pet_service');

const petController = {};

petController.Get = (app, req, res) => {
  res.status(200).send(service.Get(app));
};

petController.Post = (app, req, res) => {
  service.Post(app, req.body)
    .then((ret) => {
      res.send(ret);
    })
    .catch((err) => {
      res.status(400).json(err);
    });
};

petController.Put = (app, req, res) => {
  service.Put(app, req.body, req.body.id)
    .then((ret) => {
      res.send(ret);
    })
    .catch((err) => {
      res.status(400).json(err);
    });
};

petController.Search = (app, req, res) => {
  service.Search(app, req.params.id)
    .then((ret) => {
      res.send(ret);
    })
    .catch((err) => {
      res.status(400).json(err);
    });
};

petController.Delete = (app, req, res) => {
  service.Delete(app, req.params.id);
  res.status(204).send();
};

petController.fillValues = (app) => {
  service.fill(app);
};
module.exports = petController;
