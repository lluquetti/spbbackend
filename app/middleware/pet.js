module.exports = async (req, res, next) => {
  try {
    const token = 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjY3NmY5MmU1MGQ';
    const tokenRequest = req.headers.authorization.replace('Bearer ', '');
    if (token === tokenRequest) {
      next();
    } else {
      res.status(401).json();
    }
  } catch (e) {
    res.status(401).json(e);
  }
};
