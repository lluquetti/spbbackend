const petService = {};
const value = require('../models/pet');

petService.Get = app => app.dataBase.getAllData();

petService.Post = (app, insert) => new Promise((resolve) => {
  app.dataBase.insert(insert, (err, newDocs) => {
    resolve(newDocs);
  });
});

petService.Put = (app, update) => new Promise((resolve) => {
  const manipule = update;
  const find = update.id;
  delete manipule.id;
  app.dataBase.update({ _id: find }, manipule, () => {
    app.dataBase.find({ _id: find }, (err, ret) => {
      resolve(ret);
    });
  });
});

petService.Search = (app, id) => new Promise((resolve) => {
  app.dataBase.find({ _id: id }, (err, ret) => {
    resolve(ret);
  });
});

petService.Delete = (app, id) => {
  app.dataBase.remove({ _id: id });
};

petService.fill = (app) => {
  value.forEach((item) => {
    app.dataBase.insert(item);
  });
};
module.exports = petService;
