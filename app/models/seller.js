const productsSeller = require('./productsSeller');

const seller = [{
  id: 1,
  name: 'Seller 1',
  adrress: 'Endereço 1',
  number: 160,
  neighborhood: 'Bairro 1',
  city: 'City 1',
  state: 'State 1',
  geoLocation: [{
    lat: -22.9056,
    lont: -47.0608,
  }],
  products: productsSeller,
}];

module.exports = seller;
