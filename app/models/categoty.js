const pet = require('./pet.js');

const imagesPet = [{
  id: 1,
  petId: pet[1].id,
  date: new Date(),
  image: 'https://images.pexels.com/photos/356378/pexels-photo-356378.jpeg?auto=compress&cs=tinysrgb&h=350',
},
{
  id: 2,
  petId: pet[1].id,
  date: new Date(),
  image: 'https://images.pexels.com/photos/58997/pexels-photo-58997.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
}];

module.exports = imagesPet;
