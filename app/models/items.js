const order = require('./order');
const productSeller = require('./products_seller');
const pet = require('./pet');

const items = [{
  id: 1,
  orderId: order[0].id,
  productSeller: productSeller[0].id,
  count: 1,
  price: productSeller[0].price,
  petId: pet[0].id,
},
{
  id: 2,
  orderId: order[0].id,
  productSeller: productSeller[1].id,
  price: productSeller[1].price,
  count: 1,
  petId: pet[0].id,
}];

module.exports = items;
