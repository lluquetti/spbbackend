const categoty = [{
  id: 1,
  name: 'Cachorro',
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/categoria_1.png?alt=media&token=15e79484-61ef-4749-897f-bbbe909480a6',
},
{
  id: 2,
  name: 'Gato',
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/categoria_2.png?alt=media&token=5fdf3088-0473-4f21-87b9-0a4ad87e0ea9',
},
{
  id: 3,
  name: 'Rações',
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/categoria_3.png?alt=media&token=1ce997fd-b6af-4158-9e8b-1018798cfea2',
},
{
  id: 4,
  name: 'Coleira',
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/categoria_4.png?alt=media&token=302d7c2d-fb0e-4e8e-b1f6-aa7e8e6beb0e',
},
{
  id: 5,
  name: 'Petiscos',
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/categoria_5.png?alt=media&token=4aae0243-42bd-4dc0-b744-4906b5ede35d',
},
{
  id: 6,
  name: 'Brinquedos',
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/categoria_5.png?alt=media&token=4aae0243-42bd-4dc0-b744-4906b5ede35d',
}];

module.exports = categoty;
