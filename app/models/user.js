const plan = require('./plan');

const user = {
  id: 1,
  name: 'User 1',
  adrress: 'Endereço 1',
  number: 160,
  neighborhood: 'Bairro 1',
  city: 'City 1',
  state: 'State 1',
  count_pets: 2,
  plan: plan[0],
};

module.exports = user;
