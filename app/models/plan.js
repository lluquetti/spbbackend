const productsSeller = require('./productsSeller');

const plan = [{
  id: 1,
  name: 'Descubra seu plano',
  price: 0.00,
  description: 'Plano totalmente personalizado especialmente para o seu pet.',
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/DescrubraSeuPlano.jpeg?alt=media&token=fdb1384d-8d42-4d9b-b46b-99754cb2a6c5',
  products: [],
  recurrence: 0,
},
{
  id: 2,
  name: 'Plano Bolt',
  price: 299.00,
  description: 'Neste plano está incluso 4 serviços de banho e tosa em petshop comum e 1 pack de ração 500g',
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/PlanoBol.jpeg?alt=media&token=3ad008a9-fb1f-43c1-81c2-8adfe47a7a44',
  products: [
    productsSeller[0],
    productsSeller[4],
    productsSeller[7],
  ],
  recurrence: 30,
},
{
  id: 3,
  name: 'Plano Super Bolt (Mais selecionado)',
  recurrent: true,
  price: 399.90,
  description: 'Inclui 6 pet shops 2 pet shops premium Rações especiais',
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/PlanoSuperBolt.jpeg?alt=media&token=e0a954ee-7b56-46e5-9d6c-975def4426ba',
  products: [
    productsSeller[1],
    productsSeller[5],
    productsSeller[8],
  ],
  recurrence: 15,
},
{
  id: 4,
  name: 'Plano Bolt Premium',
  recurrent: true,
  price: 599.00,
  description: 'Inclui 10 pet shops, 5 pet shops premium, Rações especiais',
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/PlanoSuperPremium.jpeg?alt=media&token=f88810bd-fcd4-4dcb-b743-1b6be1f741cd',
  products: [
    productsSeller[2],
    productsSeller[6],
    productsSeller[9],
  ],
  recurrence: 10,
}];

module.exports = plan;
