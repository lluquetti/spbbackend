const user = require('./user');

const order = {
  id: 1,
  date: new Date(),
  userId: user[0].id,
  total: 31.98,
};

module.exports = order;
