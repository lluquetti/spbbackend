const seller = require('./seller');

const avalSeller = [{
  id: 1,
  sellerId: seller[0].id,
  date: new Date(),
  aval: 'Duis facilisis dignissim cursus. Nulla quis efficitur nibh, sed congue leo. Nullam cursus finibus erat sit amet facilisis. Duis rutrum odio at varius tempus. Praesent et pellentesque enim, ut fringilla neque. Nam at fringilla lorem. Maecenas et diam quis metus venenatis interdum. Integer quis facilisis felis. Nulla nulla lectus, rhoncus eget ante vel, euismod vehicula turpis. Aenean commodo ac erat vitae bibendum. Pellentesque ornare eleifend quam quis tempus. Mauris non suscipit eros, eget cursus mauris. Sed euismod dolor sed odio condimentum luctus. Etiam ac justo dui. Maecenas ac nibh in ex gravida dignissim.',
},
{
  id: 2,
  sellerId: seller[0].id,
  date: new Date(),
  aval: 'Duis facilisis dignissim cursus. Nulla quis efficitur nibh, sed congue leo. Nullam cursus finibus erat sit amet facilisis. Duis rutrum odio at varius tempus. Praesent et pellentesque enim, ut fringilla neque. Nam at fringilla lorem. Maecenas et diam quis metus venenatis interdum. Integer quis facilisis felis. Nulla nulla lectus, rhoncus eget ante vel, euismod vehicula turpis. Aenean commodo ac erat vitae bibendum. Pellentesque ornare eleifend quam quis tempus. Mauris non suscipit eros, eget cursus mauris. Sed euismod dolor sed odio condimentum luctus. Etiam ac justo dui. Maecenas ac nibh in ex gravida dignissim.',
}];

module.exports = avalSeller;
