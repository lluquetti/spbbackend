const seller = require('./seller');
const productSeller = require('./products_seller');

const avalProducSeller = [{
  id: 1,
  sellerId: seller[0].id,
  productSellerId: productSeller[0].id,
  date: new Date(),
  aval: 'Cras tempor ligula eu augue auctor vulputate. Nunc egestas fermentum massa vel viverra. Nulla tristique dolor et pretium dignissim. Suspendisse euismod accumsan magna et pellentesque. Fusce ut egestas nisi, nec laoreet leo. In finibus facilisis suscipit. Nulla vulputate velit libero, sit amet tristique dui consequat et. Nulla facilisi. Ut non neque euismod, malesuada augue vel, sodales risus. Aenean varius dui ac interdum pellentesque.',
},
{
  id: 2,
  sellerId: seller[0].id,
  productSellerId: productSeller[0].id,
  date: new Date(),
  aval: 'Cras tempor ligula eu augue auctor vulputate. Nunc egestas fermentum massa vel viverra. Nulla tristique dolor et pretium dignissim. Suspendisse euismod accumsan magna et pellentesque. Fusce ut egestas nisi, nec laoreet leo. In finibus facilisis suscipit. Nulla vulputate velit libero, sit amet tristique dui consequat et. Nulla facilisi. Ut non neque euismod, malesuada augue vel, sodales risus. Aenean varius dui ac interdum pellentesque.',
}];

module.exports = avalProducSeller;
