const productsSeller = [{
  id: 1,
  name: 'Banho básico',
  price: 10.00,
},
{
  id: 2,
  name: 'Banho intermediário',
  price: 25.99,
},
{
  id: 3,
  name: 'Banho premium',
  price: 35.99,
},
{
  id: 4,
  name: 'Tosa',
  price: 15.99,
},
{
  id: 5,
  name: 'Ração básica',
  price: 15.99,
},
{
  id: 6,
  name: 'Ração intermediária',
  price: 15.99,
},
{
  id: 7,
  name: 'Ração premium',
  price: 15.99,
},
{
  id: 8,
  name: 'Adicional 1',
  price: 15.99,
},
{
  id: 9,
  name: 'Adicional 2',
  price: 15.99,
},
{
  id: 10,
  name: 'Adicional 3',
  price: 15.99,
},

];

module.exports = productsSeller;
