const blog = [{
  id: 1,
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/blog1.jpeg?alt=media&token=01adf309-d715-4928-b256-f03a24811881',
  title: 'Como acabar com o mau hálito do meu cachorro?',
},
{
  id: 2,
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/blog1.jpeg?alt=media&token=01adf309-d715-4928-b256-f03a24811881',
  title: 'Como calular a idade do meu gato?',
},
{
  id: 3,
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/blog1.jpeg?alt=media&token=01adf309-d715-4928-b256-f03a24811881',
  title: 'Visite a feira de adoção no parque do povo',
},
{
  id: 4,
  image: 'https://firebasestorage.googleapis.com/v0/b/testeauthentication-5f409.appspot.com/o/blog4.jpeg?alt=media&token=4b4484e4-a730-4c81-bf40-852bcc48a80f',
  title: 'Aniversário da ONG resgata meu PET',
}];

module.exports = blog;
